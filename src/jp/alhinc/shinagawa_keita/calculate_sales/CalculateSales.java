package jp.alhinc.shinagawa_keita.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args) {
		//System.out.println("支店定義ファイルのテキストを表示します =>" + args[0]);

		BufferedReader br = null;
		try {
			//ファイルのパスと名前を指定してbranchFileオブジェクトを生成
			File branchFile = new File(args[0], "branch.lst");
			//FileReaderObjectを生成
			FileReader fr = new FileReader(branchFile);
			//BufferedReaderObjectの生成
			br = new BufferedReader(fr);

			String line;

			//支店コードと支店名を分割し、branchMap内に格納
			while ((line = br.readLine()) != null) {
				String[] branchInfo = line.split(",", 0);

				HashMap<String, String> branchMap = new HashMap<String, String>();
				branchMap.put(branchInfo[0], branchInfo[1]);
				//System.out.println(branchMap.entrySet());
			}
		} catch (IOException e) {
			if (br == null) {
				//支店定義ファイルが存在しない場合に表示
				System.out.println("支店定義ファイルが存在しません");
			}
		} finally {
			if (br == null) {
				try {
					//支店定義ファイルが存在しない場合に処理を終了する
					br.close();
				} catch (IOException e) {
					System.out.println("処理を終了できません");
				}
			}
		}
		//売上ファイルの読込
		try {
			//Fileオブジェクトと各Readerを生成
			File salesFile = new File(args[0]);
			FileReader fr = new FileReader(salesFile);
			br = new BufferedReader(fr);

			String line;

			//ディレクトリ内のファイル一覧を配列に格納
			File[] fileNames = salesFile.listFiles();

			for (int i = 0; i < fileNames.length; i++) {
				//ディレクトリ内のファイルを上から順番に参照し、条件と一致するか判定
				if (fileNames[i].getName().matches("[0-9]{8}.rcd")) {
					//取得したファイル名を確認用にコンソールに表示
					//System.out.println(fileNames[i].getName());

					while((line = br.readLine()) !=null)
					{

					}
				}
			}
		} catch (IOException e) {

		} finally {
			System.out.println("仮処理");
		}
	}
}
